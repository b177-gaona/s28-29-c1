// Create an Express.js server that will login a user.

// Create a folder named express-activity and inside initialize it to npm. Install the express JS package
// Create a .gitignore file to ignore the node_modules directory when pushing.

// Create an index.js file that will serve as the app's entry point and setup a simple server running in port 4001.

const express = require("express");
const app = express();
const port = 4001;
app.use(express.json());
app.use(express.urlencoded({extended:true}));

// GET / - Test
app.get("/", (req, res) => {
    res.send("Hello, world!")
})

// GET /users - Test
app.get("/users", (req, res) => {
    res.send(users);
});

// Create an array to use as your mock database.
let users = [];

// Create a POST route to register a user with it's firstname, lastname, username, and password.
app.post("/signup", (req, res) => {
    console.log(req.body);
    if(req.body.firstname !== '' && req.body.lastname !== '' && req.body.username !== '' && req.body.password !== '') {
        users.push(req.body);
        res.send(`User ${req.body.username} succesfully registered`);
    }
    else{
        res.send("Please input BOTH username & password.");
    }
});

// Process a POST request using postman.
// Solution: Refer to 'Express POST /signup' on WDC028-29-C1 Node Collection.postman_collection.json

// Create another POST route to login a user with its username and password.
app.post("/login", (req, res) => {
    let message;
    for(let i=0; i<users.length; i++) {
        if(req.body.username == users[i].username) {
            if(req.body.password!==users[i].password) {
                message = `Unauthorized login for ${req.body.username}! The password you entered is incorrect!`;
            } else {
                message = `Successfully logged in user ${req.body.username}.`;
            }            
        } else {
            message = "User does not exist.";
        }
    }
    res.send(message);
})

// Process the POST request using postman.
// Solution: Refer to 'Express POST /login with unknown user', 'Express POST /login with incorrect password' & 'Express POST /login with valid username & password' on WDC028-29-C1 Node Collection.postman_collection.json

// Export the postman collection and save it in the express-acitvity folder.
// Solution: Refer to WDC028-29-C1 Node Collection.postman_collection.json

// Always include this line at the bottom
app.listen(port, () => console.log(`Server running at port ${port}`));