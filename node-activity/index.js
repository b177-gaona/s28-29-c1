// Create a Node.js application that will retrieve a single post item from JSON placeholder API.

// Create a node-activity folder and inside, create and index.html and index.js.
// Link the index.js to the index.html file.

// Using the JSON placeholder "https://jsonplaceholder.typicode.com/posts", create a fetch request using GET method that will retrieve a single to do list item.
// Using the data retrieved, print a message in the console that will provide the title and body of the post item.
fetch('https://jsonplaceholder.typicode.com/posts/1', {
    method: 'GET'
})
.then((response) => response.json())
.then((json) => console.log(
    `The item "${json.title}" has a post of "${json.body}"`
    ));

// Create a fetch request using PATCH method to update a post item using JSON placeholder API.
// Using the same index.js file, create a fetch request using PATCH method that will update a post item using the same JSON placeholder.
// Update a post item by changing the title and the body.

fetch('https://jsonplaceholder.typicode.com/posts/1', {
    method: 'PATCH',
    headers: {
        'Content-type': 'application/json',
    },
    body: JSON.stringify({
            title: "new title",
            body: "new body"
    }),
})
.then((response) => response.json())
.then((json) => console.log(json));

// Create the requests via Postman to retrieve and update a post.
// Solution: See WDC028-29-C1 Collection.postman_collection.json